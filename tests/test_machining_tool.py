import unittest
from cnc.tools.machining_tool import MachiningTool


class TestMachingTool(unittest.TestCase):
    def setUp(self):
        self.mill = MachiningTool(length=50.0,
                             shaft_diameter=8.0,
                             name='50mm end mill')

    def tearDown(self):
        del(self.mill)

    def test_init(self):
        self.assertEqual(self.mill.name, '50mm end mill')
        self.assertAlmostEqual(self.mill.length, 50.0)

    def test_shaft_diameter_setter_getter(self):
        self.mill.shaft_diameter = 30.0
        self.assertAlmostEqual(self.mill.shaft_diameter, 30.0)

        with self.assertRaises(ValueError):
            self.mill.shaft_diameter = -4.001