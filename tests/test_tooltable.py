import unittest
from unittest.mock import Mock
from cnc import ToolTable
from cnc.tools import MillingTool



class TestToolTable(unittest.TestCase):
    def setUp(self):
        mill = MillingTool(id=1,
                           length=30.0,
                           shaft_diameter=8.0,
                           diameter=3.0,
                           name='3mm end mill')
        self.tool_table = ToolTable()
        self.tool_table.add_tool(mill)

    @unittest.mock.patch('builtins.open')
    def test_save(self, mock_open):
        self.tool_table.save("tooltable.json")
        mock_open.assert_called_with("tooltable.json", 'w')
