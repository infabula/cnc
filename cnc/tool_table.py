import json


class ToolTable:
    def __init__(self):
        self.tools = []

    def create_tool(self, tooltype, properties):
        # Factory function
        #mill = MillingTool()
        #return mill
        pass

    def add_tool(self, tool):
        self.tools.append(tool)

    def remove_tool(self, name):
        pass

    def print_tools(self):
        for tool in self.tools:
            print('-', tool.name)

    def save(self, filename):
        dump_list = []
        for tool in self.tools:
            dump_list.append(tool.__dict__)

        with open(filename, 'w') as outfile:
            json_representation = json.dumps(dump_list, indent=2)
            outfile.write(json_representation)

    def __bool__(self):
        return self.tools

    def __repr__(self):
        return "Tooltable"