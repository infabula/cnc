class Spindle:
    def __init__(self):
        self.rpm = 0

    def set_rpm(self, rpm):
        self.rpm = int(rpm)

    def __enter__(self):
        print("Activating the spindle.")
        return self  # don't forget!

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.rpm = 0
        print("Setting the rpm to", self.rpm)
