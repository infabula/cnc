class Conversion:
    PI = 3.14159

    @classmethod
    def radians_to_degrees(cls, rad):
        return (rad * 180.0) / cls.PI

    @staticmethod
    def degrees_to_fahrenheit(deg):
        return (deg * (9.0 / 5.0)) + 32.0
