""" Base class for Machine tools.
"""
import json


class MachiningTool():
    """Base class for Machine tools.
    """
    def __init__(self, **kwargs):
        self.name = kwargs.get('name', 'unknown')
        self.id = kwargs.get('id', -1)
        if 'length' in kwargs:
            self.length = kwargs['length']

        else:
            raise ValueError("Length not provided.")

        if 'shaft_diameter' in kwargs:
            self.shaft_diameter = kwargs['shaft_diameter']
        else:
            raise ValueError("Shaft_diameter not provided.")

    def __ge__(self, other):
        return self.length >= other.length

    def __repr__(self):
        return f"{self.name} {self.length}"
        #return "{name} ({length})".format(name=self.name, length=self.length)


    @property  # getter
    def shaft_diameter(self):
        return self._shaft_diameter

    @shaft_diameter.setter
    def shaft_diameter(self, diameter):
        if diameter > 0:
            self._shaft_diameter = diameter
        else:
            raise ValueError("Tool length can't be negative")

    def get_length(self):
        return self._length

    def set_length(self, length):
        if length > 0:
            self._length = length
        else:
            raise ValueError("Tool length can't be negative")

    length = property(get_length, set_length)

    def save(self):
        filename = f"machiningtool_{self.id}.json"
        with open(filename, 'w') as outfile:
            json_representation = json.dumps(self.__dict__, indent=2)
            outfile.write(json_representation)

    def serialize(self):
        json_representation = json.dumps(self.__dict__, indent=2)
        return json_representation

if __name__ == "__main__":
    mill_1 = MachiningTool(length=50.0,
                           shaft_diameter=8.0,
                          name='50mm end mill')
