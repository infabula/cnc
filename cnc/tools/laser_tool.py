from .machining_tool import MachiningTool


class LaserTool(MachiningTool):
    def __init__(self, **kwargs):
        if 'power' in kwargs:
            self.power = kwargs['power']
        else:
            raise ValueError("power not provided")

        super().__init__(**kwargs)
