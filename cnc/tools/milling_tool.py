from .machining_tool import MachiningTool


class MillingTool(MachiningTool):
    def __init__(self, **kwargs):
        if 'diameter' in kwargs:
            self.diameter = kwargs['diameter']
        else:
            raise ValueError("diameter not provided")
        self.nb_of_blades = kwargs.get('nb_of_blades', 2)

        super().__init__(**kwargs)


if __name__ == "__main__":
    mill = MillingTool(length=50.0,
                       shaft_diameter=8.0,
                       diameter = 3.0,
                       name='3mm end mill',
                       id=42)
