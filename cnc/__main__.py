from .server import run_server
from .cnc import Cnc
from .tools.milling_tool import MillingTool
from .tools.laser_tool import LaserTool
from .tool_table import  ToolTable


def run_cnc():
    print("CNC is working.")
    mill = MillingTool(id=1,
                       length=30.0,
                       shaft_diameter=8.0,
                       diameter=3.0,
                       name='3mm end mill')
    print("Diameter", mill.diameter)
    mill.length = 20.0
    print("Length", mill.length)


    laser = LaserTool( id=2,
                       length=40.0,
                       shaft_diameter=8.0,
                       power=1.5,
                       name='laser')

    if mill >= laser:
        print("Mill hits the floor earlier.")

    tool_table = ToolTable()
    tool_table.add_tool(mill)
    tool_table.add_tool(laser)

    tool_table.print_tools()
    tool_table.save("tooltable.json")

    cnc = Cnc(name="Woodpegger")
    cnc.turn_on()
    cnc.do_work()

def main():
    run_server()

main()