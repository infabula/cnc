from flask import Flask, render_template, request
from flask_restful import Resource, Api
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
api = Api(app)
#app.config['SQLALCHEMY_DATABASE_URI'] =  'sqlite:////tmp/test.db'
app.config['SQLALCHEMY_DATABASE_URI'] =  "sqlite:///:memory:"

db = SQLAlchemy(app) #


class Tool(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    length = db.Column(db.Float, default=0.0)

    ##def __init__(self, name, length):
    #    pass

    def __repr__(self):
        return f"<Tool> {self.id} {self.name}"



def run_server():
    db.create_all()

    tool1 = Tool(name="3mm endmill", length=40.0)
    tool2 = Tool(name="6mm endmill", length=50.0)

    db.session.add(tool1)
    db.session.add(tool2)
    db.session.commit()

    #print(tool1)

    fixture_db = {
        'tooltable_name': "CNC music factory",
        'tools': { '1': {'id': 1,
                         'name': "3mm endmill",
                         'length': 40.0
                         },
                   '2': {'id': 2,
                         'name': "6mm endmill",
                         'length': 50.0}
                }
    }

    @app.route('/hello')
    def hello_world():
        return 'Hello, World!'

    @app.route('/tools')
    def show_tools():

        return render_template('tools.j2', **fixture_db)

    @app.route('/tools/<id>')
    def show_tool_details(id):
        tool_data = fixture_db['tools'].get(id, {})

        return f"{tool_data}"

    ######################## REST ###################

    class APITool(Resource):
        def get(self, id):
            tool = Tool.query.filter_by(id=id).first()
            #tool_data = fixture_db['tools'].get(str(id), {})
            data = {'id': tool.id,
                    'name': tool.name,
                    'length': tool.length}
            return data

        def put(self, id):
            if fixture_db['tools'][str(id)]:
                tool = fixture_db['tools'][str(id)]
                data = request.form['length']
                tool['length'] = request.form.get('length', tool['length'])
                return tool

    api.add_resource(APITool, '/api/tools/<int:id>')

    app.run(debug=True)
