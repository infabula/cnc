"""
CNC machine class
"""
from .spindle import Spindle

class Cnc:
    def __init__(self, name):
        self.name = name
        self.is_on = False

    def turn_on(self):
        """Turns the machine on.

        No arguments required.

        """
        self.is_on = True

    def do_work(self):
        with Spindle() as spindle:
            spindle.set_rpm(5000)
            print("Spindle runs at", spindle.rpm)
            # do more work
